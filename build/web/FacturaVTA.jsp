<%-- 
    Document   : PedidoVTA
    Created on : 24/08/2020, 01:22:25 AM
    Author     : JONATHAN
--%>

<%@page import="modelo.dto.UsuarioDTO"%>
<%@page import="controlador.Facade"%>
<%@page import="java.util.List"%>
<%@page import="modelo.dto.SesionDTO"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<%
    //Codigo para validar sesion y darle seguridad de accseso:
    HttpSession objLogin = request.getSession(); //Se obtiene el objeto de la sesion
    SesionDTO usuRegistrado = (SesionDTO) objLogin.getAttribute("login"); //Se obtiene el usuario que entra a la sesion
    String Nombre = "";

    Facade objf = new Facade();
    //Me trae la lista de los usuarios para obtener su nombre: 
    List<UsuarioDTO> lis = objf.listarUsuarios();
    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
    List<SesionDTO> lis2 = objf.listarSesiones();
    int id = 0;

    for (int i = 0; i < lis.size(); i++) {
        for (int j = 0; j < lis2.size(); j++) {
            if (lis2.get(j).getCorreo().equals(usuRegistrado.getCorreo())) {
                //Rescato el id que tiene registrado ese correo en sesiones:
                id = lis2.get(j).getId_usu();
            }
        }
        //Busco en la lista de usuarios ese id que rescate al que le pertenece el correo:
        if (lis.get(i).getId_usu() == id) {
            Nombre = lis.get(i).toString(); //Ya tengo el nombre del usuario de la sesion
        }
    }
    //Se verifica la sesion    
    if (usuRegistrado == null) //S� el usuario no esta registrado o no existe
    {
        response.sendRedirect("LoginVTA.jsp"); //SendRedirect se usa en los .jsp para redirigir y getRequestDispatcher en los servlets
    } else //S� el usuario si esta registrado entonces entra al else
    { //Este else cierra al final del cogido html    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Nuestros Productos</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
             <div style="margin-top:20px" class="row">
                <div class="col-sm-12">
                    <center>
                        <font face="georgia" size="30" color="blue">Factura generada</font>
                    </center>                  
                </div>
            </div>
            <div style="margin-top:20px" class="row">
                <div class="card col-sm-6 col-md-6 col-lg-12"> <%--Para darle bordes a la tabla de datos--%>
                    <table class="table table-dark">
                        <thead>
                            <tr class="bg-primary">
                                <th>Producto</th>
                                <th>Id Pedido</th>
                                <th>Id Producto</th>
                                <th>Nombre Producto</th>
                                <th>Unidades Producto</th>
                                <th>Valor Producto</th>
                                <th>IVA Producto</th>
                                <th>TOTAL A PAGAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="objP" items="${Lista_factura}">
                                <tr>
                                     <td>
                                        <img src="${objP.getImagen()}" height=100 width=100">                                       
                                    </td>
                                    <td>${objP.getId_pedido()}</td>
                                    <td>${objP.getId_pro()}</td>
                                    <td>${objP.getNombre_pro()}</td>
                                    <td>${objP.getUnidades_pro()}</td>                                
                                    <td>${objP.getValor_pro()}</td> 
                                    <td>${objP.getIva_pro()}</td>
                                    <td>${objP.getTotal()}</td>
                                </tr>
                        </c:forEach>                  
                        </tbody>
                    </table>          
                    <font face="georgia" size="15" color="red">Valor Total de su pedido es: ${Total_neto}</font>
                    <a class="btn btn-danger" href="FacCTO?menu=Factura&accion=Actualizar"target="Frame">Solicitar pedido</a>                                                                                                                                         
                </div>
            </div>           
        </div>

        <%--Esta linea es para utilizar codigo JavaScript--%>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
<%} //Cierra else%> 
