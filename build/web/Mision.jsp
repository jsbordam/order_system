<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mision</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    </head>
    <body>
        <div class="title text-center py-5">
            <h1 class="display-1">MISION</h1>
        </div>

        <div class="row mx-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Corona botella grande.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>                    
                </div>
            </div>

            <div class="col mx-5">
                <div class="card" style="width: 25rem;">
                    <div class="title text-center">
                        <div class="bg-primary text-white">
                            <h3>                  
                                Brindar a nuestros clientes en licores, alcoholes y productos afines, con calidad constante en la atención 
                                y el servicio satisfagan las necesidades y expectativas de los consumidores, despertando emociones y 
                                sensaciones, bajo criterios de responsabilidad social.
                            </h3> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col mx-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Ron Santa Fe 12A.jpg" class="rounded mx-auto d-block" alt="" height=420 width=350>
                </div>
            </div> 
        </div>





        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    </body>
</html>
