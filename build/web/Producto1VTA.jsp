<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Inico</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="META-INF/cards.css">
    </head>


    <body> 

        <div class="title text-center py-5">
            <h1 class="display-1">CATALOGO CCJ MARKET</h1>
            <h2 class="display-4">CERVEZA</h2>
        </div>

        <div class="row mx-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Poker Botella.jpg" class="rounded mx-auto d-block" alt="Poker Botella" height=320 width=180 >  
                    <div class="card-body">
                        <h4 class="card-title">Poker 330ml</h4>
                        <p class="card-text">Precio: 2.500</p>
                        <a href="#ventana1" class="btn btn-primary" data-toggle="modal">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Poker Lata.jpg" class="rounded mx-auto d-block" alt="Poker Lata" height=320 width=300> 
                    <div class="card-body">
                        <h4 class="card-title">Poker en lata 330ml</h4>
                        <p class="card-text">Precio: 2.000</p>
                        <a href="#ventana1" class="btn btn-primary" data-toggle="modal">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Poker Botellon.jpg" class="rounded mx-auto d-block" alt="Pokeron" height=320 width=280>  
                    <div class="card-body">
                        <h4 class="card-title">Pokeron 2500ml</h4>
                        <p class="card-text">Precio: 4.000</p>
                        <a href="#ventana1" class="btn btn-primary" data-toggle="modal">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>

        <%--Esta es la tarjeta flotante--%>        
        <div class="modal fade" id="ventana1">                
            <div class="modal-dialog" height=200 width=200>
                <div class="modal-content">
                    <%--Header de la ventana--%>  
                    <div class="modal-header">                           
                        <h4 class="modal-tittle">¡TIENES QUE INICIAR SESION!</h4>
                    </div>
                    <form action="ProductoCTO?menu=Producto" method="POST"> 
                        <%--Contenido de la ventana--%>
                        <div class="modal-body">
                            <img src="imagenes/Triste.jpg"  height=200 width=300>
                            <label>Si no tienes una cuenta, te puedes registrar...</label>
                        </div>
                        <%--Footer de la ventana--%>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Aguila Botella.jpg" class="rounded mx-auto d-block" alt="Aguila Botella" height=320 width=240>   
                    <div class="card-body">
                        <h4 class="card-title">Aguila 330ml</h4>
                        <p class="card-text">Precio: 2.500</p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Aguila Lata.jpg" class="rounded mx-auto d-block" alt="Aguila Lata" height=320 width=280>
                    <div class="card-body">
                        <h4 class="card-title">Aguila en lata 330ml</h4>
                        <p class="card-text">Precio: 2.000</p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Aguila Light Botella.jpg" class="rounded mx-auto d-block" alt="Aguila Light" height=320 width=200> 
                    <div class="card-body">
                        <h4 class="card-title">Aguila Light 330ml</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Club colombia botella.jpg" class="rounded mx-auto d-block" alt="Club Colombia" height=320 width=240>   
                    <div class="card-body">
                        <h4 class="card-title">Club Colombia 330ml </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Club colombia lata.jpg" class="rounded mx-auto d-block" alt="Club Colombia en lata" height=320 width=150>
                    <div class="card-body">
                        <h4 class="card-title"> Club Colombia en lata</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Costeña botella.jpg" class="rounded mx-auto d-block" alt="Costeña" height=320 width=240>  
                    <div class="card-body">
                        <h4 class="card-title">Costeña 330ml </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Costeña lata.jpg" class="rounded mx-auto d-block" alt="Costeña en lata" height=320 width=280>
                    <div class="card-body">
                        <h4 class="card-title">Costeña en lata</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Corona botella grande.jpg" class="rounded mx-auto d-block" alt="Corona" height=420 width=280>  
                    <div class="card-body">
                        <h4 class="card-title">Corona 330ml </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Corona botella pequeña.jpg" class="rounded mx-auto d-block" alt="Coronita" height=380 width=200>
                    <div class="card-body">
                        <h4 class="card-title">Coronita</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Budweiser botella.jpg" class="rounded mx-auto d-block" alt="" height=320 width=240>  
                    <div class="card-body">
                        <h4 class="card-title">Budweiser 330ml </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Budweiser lata.jpg" class="rounded mx-auto d-block" alt="" height=320 width=300>
                    <div class="card-body">
                        <h4 class="card-title">Budweiser en lata</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Heineken botella.jpg" class="rounded mx-auto d-block" alt="" height=400 width=340>  
                    <div class="card-body">
                        <h4 class="card-title">Heineken </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Heineken lata.jpg" class="rounded mx-auto d-block" alt="" height=380 width=150>
                    <div class="card-body">
                        <h4 class="card-title">Heineken en lata</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>


        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Redds botella.jpg" class="rounded mx-auto d-block" alt="" height=420 width=300> 
                    <div class="card-body">
                        <h4 class="card-title">Redds </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Redds lata.jpg" class="rounded mx-auto d-block" alt="" height=380 width=280>
                    <div class="card-body">
                        <h4 class="card-title"> Redds en lata</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>



        <div class="title text-center py-5">
            <h2 class="display-4">LICORES FUERTES</h2>
        </div>


        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Ron Caldas Blanco.jpg" class="rounded mx-auto d-block" alt="" height=320 width=240> 
                    <div class="card-body">
                        <h4 class="card-title">Ron Caldas Balnco </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Ron Santa Fe 12A.jpg" class="rounded mx-auto d-block" alt="" height=320 width=280>
                    <div class="card-body">
                        <h4 class="card-title">Ron Santa Fe 12 Años</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Tequila Blanco.jpg" class="rounded mx-auto d-block" alt="" height=380 width=300> 
                    <div class="card-body">
                        <h4 class="card-title">Tequila Blanco </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Whiskey Bourbon Evan Williams.jpeg" class="rounded mx-auto d-block" alt="" height=480 width=360>   
                    <div class="card-body">
                        <h4 class="card-title">Whiskey Bourbon Evan Williams </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Whisky Johnny Walker Red Label.jpg" class="rounded mx-auto d-block" alt="" height=450 width=280>
                    <div class="card-body">
                        <h4 class="card-title">Whisky Johnny Walker Red Label </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vodka.jpg" class="rounded mx-auto d-block" alt="" height=480 width=340> 
                    <div class="card-body">
                        <h4 class="card-title">Vodka </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Amaretto.jpeg" class="rounded mx-auto d-block" alt="" height=420 width=340>  
                    <div class="card-body">
                        <h4 class="card-title">Amaretto </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Angostura.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Angostura </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Campari.jpg" class="rounded mx-auto d-block" alt="" height=420 width=300>
                    <div class="card-body">
                        <h4 class="card-title">Campari </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Cointreau.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>  
                    <div class="card-body">
                        <h4 class="card-title">Cointreau </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Ginebra London.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Ginebra London </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Gin London Dry.jpeg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Gin London Dry </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vermount.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>  
                    <div class="card-body">
                        <h4 class="card-title">Vermount </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vermouth Blanco Seco.png" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Vermount Balnco Seco</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vermouth Rojo Dulce.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340> 
                    <div class="card-body">
                        <h4 class="card-title">Vermount Rojo Dulce </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="title text-center py-5">
            <h2 class="display-4">VINOS</h2>
        </div>


        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Canto de Apalta.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>   
                    <div class="card-body">
                        <h4 class="card-title">Vino Canto de Apalta </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Gato Blanco.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Vino Gato Blanco </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Libertador.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Vino Libertador </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Luigi Bosca.jpeg" class="rounded mx-auto d-block" alt="" height=420 width=340> 
                    <div class="card-body">
                        <h4 class="card-title">Vino Luigi Bosca </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Outer Limits.jpg" class="rounded mx-auto d-block" alt="" height=420 width=380>
                    <div class="card-body">
                        <h4 class="card-title">Vino Outer Limits</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Volcanes.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340> 
                    <div class="card-body">
                        <h4 class="card-title">Vino Volcanes </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>  
        </div>



        <div class="row mx-5 py-5">
            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Cariñoso.jpg" class="rounded mx-auto d-block" alt="" height=420 width=300>  
                    <div class="card-body">
                        <h4 class="card-title">Vino Cariñoso </h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="col ml-5">
                <div class="card" style="width: 20rem;">
                    <img src="imagenes/Vino Casa del Rin.jpg" class="rounded mx-auto d-block" alt="" height=420 width=340>
                    <div class="card-body">
                        <h4 class="card-title">Vino Casa del Rin</h4>
                        <p class="card-text">Precio: </p>
                        <a href="#" class="btn btn-primary">Comprar</a>
                    </div>
                </div>
            </div> 
        </div>


        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    </body>
</html>
