<%-- 
    Document   : LoginVTA
    Created on : 21/08/2020, 10:49:52 PM
    Author     : JONATHAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Iniciar Sesion</title>
        <%--Esta linea me añade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
     </head>
       <body>
        <img src="https://www.upsocl.com/wp-content/uploads/2020/01/portada-jameson.jpg" style="filter: blur(3px);width: 100vw; max-height: 100vh; position: absolute; z-index: -1;">
        <div style="width: 100vw; height: 100vh;">
            <div class="modal-dialog text-center">
                <div class="col-lg-8">  
                    <div class="modal-content"style="width: 25vw;margin: auto; top: 25%; background-color: #ffffff8f;">
                        <div class="card p-3" style="width: 25vw;margin: auto; top: 25%; background-color: #ffffff8f;">
                            <div class="col-12 p-0" style="text-align: center;">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2zXkhxe6yxS3V2UG68B19VO15nIhc6Jp0TA&usqp=CAU" height="150" width="150"/>
                            </div> 
                            <form action="LoginCTO?accion=ValidaLogin" method="POST">
                                <div class="form-group text-center">
                                    <p>
                                        <strong>Bienvenidos al sistema de Login</strong>
                                    </p>
                                    <hr style="border-top: 1% solid #bbb">
                                </div> 
                                <div class="form-group text-center">
                                    <label>Usuario: </label>
                                    <input type="text" name="txt_usuario" placeholder="ejemplo@gmail.com" class="form-control" required>
                                </div>
                                <div class="form-group text-center">
                                    <label>Clave: </label>
                                    <input type="password" name="txt_clave" placeholder="password" class="form-control" required>
                                </div> 
                                <input type="submit" name="btn_ingresar" value="Ingresar" class="btn btn-primary">
                                <a class="btn btn-secondary" style="margin-right: 10px;border: none"  href="LoginCTO?accion=Registrarse" target="_blank">Registrarse</a>
                            </form>
                        </div>
                        <a class="btn btn-info"style="margin-right: 0px;border: none" href="index.jsp">Pagina principal</a> 

                    </div>
                </div>
            </div>
        </div>

        <%--Esta linea es para utilizar codigo JavaScript--%>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
