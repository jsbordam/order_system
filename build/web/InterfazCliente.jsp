<%-- 
    Document   : InterfazUsuario
    Created on : 21/08/2020, 11:29:46 PM
    Author     : JONATHAN
--%>
<%@page import="modelo.dto.UsuarioDTO"%>
<%@page import="controlador.Facade"%>
<%@page import="java.util.List"%>
<%@page import="modelo.dto.SesionDTO"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<%
    //Codigo para validar sesion y darle seguridad de accseso:
    HttpSession objLogin = request.getSession(); //Se obtiene el objeto de la sesion
    SesionDTO usuRegistrado = (SesionDTO) objLogin.getAttribute("login"); //Se obtiene el usuario que entra a la sesion
    String Nombre = "";

    Facade objf = new Facade();
    //Me trae la lista de los usuarios para obtener su nombre: 
    List<UsuarioDTO> lis = objf.listarUsuarios();
    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
    List<SesionDTO> lis2 = objf.listarSesiones();
    int id = 0;

    for (int i = 0; i < lis.size(); i++) {
        for (int j = 0; j < lis2.size(); j++) {
            if (lis2.get(j).getCorreo().equals(usuRegistrado.getCorreo())) {
                //Rescato el id que tiene registrado ese correo en sesiones:
                id = lis2.get(j).getId_usu();
            }
        }
        //Busco en la lista de usuarios ese id que rescate al que le pertenece el correo:
        if (lis.get(i).getId_usu() == id) {
            Nombre = lis.get(i).toString(); //Ya tengo el nombre del usuario de la sesion
        }
    }
    //Se verifica la sesion    
    if (usuRegistrado == null) //S� el usuario no esta registrado o no existe
    {
        response.sendRedirect("LoginVTA.jsp"); //SendRedirect se usa en los .jsp para redirigir y getRequestDispatcher en los servlets
    } else //S� el usuario si esta registrado entonces entra al else
    { //Este else cierra al final del cogido html   

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>TU CUENTA</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-sm navbar-ligth bg-primary"> 
            <div id="navbarNav" class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">                       
                        <a class="btn btn-outline-dark" style="margin-left: 10px;border: none" href="CuentaClienteCTO?menu=Cuenta&accion=Listar" target="Frame">Cuenta</a>                   
                    </li>

                    <li class="nav-item">
                        <a class="btn btn-outline-dark" style="margin-left: 10px;border: none"  href="ProductoCTO?menu=Producto&accion=Listar" target="Frame">Productos</a>
                    </li > 

                    <li class="nav-item">                       
                        <a class="btn btn-outline-dark" style="margin-left: 10px;border: none" href="FacCTO?menu=Factura&accion=Generar" target="Frame">T� pedido</a>                   
                    </li>                                    
                </ul>
            </div>
            <div class="dropdown">
                <a href="#" class="btn btn-outline-dark dropdown-toggle"
                   style="margin-right: 40px; border: none" data-toggle="dropdown" >Mi cuenta
                </a>
                <div class="dropdown-menu text-center">
                    <a><%=Nombre%></a>
                    <a><%=usuRegistrado.getCorreo()%></a>
                    <div class="dropdown-divider"></div>
                    <a href="LoginCTO?accion=Salir">Cerrar sesion</a>
                </div>
            </div>
        </nav>
                    
        <h1 style="margin-top:40px">BIENVENIDO ESTIMADO CLIENTE</h1>

        <%--Esta linea es para navegar en la misma pesta�a y que no se cree otra al pasar a una vista--%>
        <div style="height: 830px;">
            <iframe name="Frame" style="height: 100%;width: 100%;border: 2px"></iframe>
        </div>

        <%--Esta linea es para utilizar codigo JavaScript--%>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
<%} //Cierra else%> 