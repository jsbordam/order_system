<%-- 
    Document   : Registrarse
    Created on : 21/08/2020, 11:40:09 PM
    Author     : JONATHAN
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Registro</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
        <img src="https://www.upsocl.com/wp-content/uploads/2020/01/portada-jameson.jpg" style="filter: blur(3px);width: 100vw; max-height: 100vh; position: absolute; z-index: -1;">
        <div style="width: 100vw; height: 100vh;">
        <<div class="container" style="margin: auto; top: 25%; background-color: #ffffff8f;">
            <div class="row">
                <div class="col-sm-12">
                                   
                    <h1 style="color: cornsilk" >Registrarse en CCJ LICORES</h1>                
                </div>
            </div>
            <div class="row">
                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de ingreso de datos--%>
                    <div class="card-body">
                        <form action="UsuarioCTO?menu=Usuario" method="POST"> 
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Nombres:</label>
                                    <input class="form-control" type="text" name="txt_nombres" required value="${usuario.getNombres()}">
                                    <input type="hidden" name="txt_id_usu" value="${usuario.getId_usu()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Apellidos:</label>
                                    <input class="form-control" type="text" name="txt_apellidos" required value="${usuario.getApellidos()}"> 
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="inputState">Tipo Documento:</label>
                                    <select id="inputState" class="form-control" name="txt_tipo_docto" required value="${usuario.getTipo_docto()}">
                                        <option>Cedula de ciudadania</option>
                                        <option>Tarjeta de identidad</option>
                                        <option>Carnet de extranjeria</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>N� Documento:</label>
                                    <input class="form-control" type="text" name="txt_num_docto" required value="${usuario.getNum_docto()}">
                                </div>                                 
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Ciudad de residencia:</label>
                                    <input class="form-control" type="text" name="txt_ciudad_residencia" required value="${usuario.getCiudad_residencia()}"> 
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Direcci�n:</label>
                                    <input class="form-control" type="text" name="txt_direccion" required value="${usuario.getDireccion()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="inputState">Rol:</label>
                                    <select id="inputState" class="form-control" name="txt_rol" required value="${usuario.getRol()}">                                       
                                        <option>Cliente</option>
                                    </select>
                                </div>                                                       
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Correo:</label>
                                    <input class="form-control" type="text" name="txt_correo" required value="${sesion.getCorreo()}">
                                </div>   

                                <div class="form-group col-sm-3">
                                    <label>Clave:</label>
                                    <input class="form-control" type="text" name="txt_clave" required value="${sesion.getClave()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Estado:</label>
                                    <input class="form-control" type="text" name="txt_estado" required value="${sesion.getEstado()}">
                                </div>
                            </div>

                            <input class="btn btn-success" type="submit" name="accion" value="Registrarse" >
                        </form>
                    </div>
                </div>
                
            </div>
            <%--Esta linea es para utilizar codigo JavaScript--%>
            
        </div>
        </div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
