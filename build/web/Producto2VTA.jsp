<%-- 
    Document   : ProductoVTA1
    Created on : 21/08/2020, 03:35:33 PM
    Author     : JONATHAN
--%>

<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-15"%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Nuestros Productos</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>        
        <div class="container">
            <div style="margin-top:20px" class="row">
                <div class="col-sm-12">
                    <center>
                        <font face="georgia" size="30" color="blue">Conoce nuestros productos</font>
                    </center>                  
                </div>
            </div>
            <div style="margin-top:20px" class="row">
                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de datos--%>
                    <table class="table table-hover">
                        <thead>
                            <tr class="bg-primary"> 
                                <th>Presentaci�n</th>
                                <th>Nombre</th>
                                <th>Descripci�n</th>
                                <th>Unidades Disponibles</th>
                                <th>Valor con IVA</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="objP" items="${Lista_productos}">
                                <tr>
                                    <td>
                                        <img src="${objP.getImagen()}" height=100 width=100">                                       
                                    </td>
                                    <td>${objP.getNombre_pro()}</td>
                                    <td>${objP.getDescripcion_pro()}</td>
                                    <td>${objP.getUnd_dis()}</td>
                                    <td>${objP.getValor_pro()}</td>  
                                    <td>     
                                        <a class="btn btn-warning" href="ProductoCTO?menu=Producto&accion=Agregar&id=${objP.getId_pro()}" target="Frame">A�adir al carrito</a>                                                                                                                                         
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>                     
                </div>
            </div>                 

            <%--Esta linea es para utilizar codigo JavaScript--%>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        </div>
    </body>
</html>
