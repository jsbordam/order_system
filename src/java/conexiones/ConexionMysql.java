
package conexiones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author JONATHAN
 */
public class ConexionMysql 
{
    public static ConexionMysql instance; //Patron Singleton
    private Connection cnn;
    
    private ConexionMysql ()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver"); //Driver de la conexion BD
            try 
            {
                cnn = DriverManager.getConnection("jdbc:mysql://localhost:3307/bd_pedidos", "root", "");
            } 
            catch (SQLException ex) 
            {
                System.out.println("Error al conectar BD: " + ex.getMessage());
            }
        } 
        catch (ClassNotFoundException ex) 
        {
            System.out.println("Error al conectar BD: " + ex.getMessage());
        }
    }
    
    public static synchronized ConexionMysql getInstance () //synchronized para acceso sincronizado
    {
        if (instance == null)
        {
            instance = new ConexionMysql();
        }
        return instance;
    }
    
    public Connection getCnn ()
    {
        return cnn;
    }
    
    public void cerrarConexion ()
    {
        instance = null;
    }  
}
