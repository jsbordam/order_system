
package modelo.dto;

/**
 *
 * @author JONATHAN
 */
public class Detalle_pedidoDTO 
{
    private int id_pedido;
    private int id_prod;
    private int cantidad_und;

    
    public Detalle_pedidoDTO() 
    {
        //Constructor vacio
    }

    //Constructor con todos los atributos:
    public Detalle_pedidoDTO(int id_pedido, int id_prod, int cantidad_und) 
    {
        this.id_pedido = id_pedido;
        this.id_prod = id_prod;
        this.cantidad_und = cantidad_und;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_prod() {
        return id_prod;
    }

    public void setId_prod(int id_prod) {
        this.id_prod = id_prod;
    }

    public int getCantidad_und() {
        return cantidad_und;
    }

    public void setCantidad_und(int cantidad_und) {
        this.cantidad_und = cantidad_und;
    }

    
     @Override
    public String toString()
    {
        return this.id_pedido+" "+this.id_prod+" "+this.cantidad_und;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Detalle_pedidoDTO other = (Detalle_pedidoDTO) obj;
        if (this.id_pedido != other.id_pedido) {
            return false;
        }
        if (this.id_prod != other.id_prod) {
            return false;
        }
        if (this.cantidad_und != other.cantidad_und) {
            return false;
        }
        return true;
    }   
}
