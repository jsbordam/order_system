
package modelo.dto;

import java.util.Objects;

/**
 *
 * @author JONATHAN
 */
public class SesionDTO
{
    private int id_usu;
    private String correo;
    private String clave;
    private int estado;

    public SesionDTO() 
    {
        //Constructor vacio
    }

    //Constructor con todos los atributos, excepto el id_usu:
    public SesionDTO(String correo, String clave, int estado) 
    {
        this.correo = correo;
        this.clave = clave;
        this.estado = estado;
    }

     //Constructor con todos los atributos:
    public SesionDTO(int id_usu, String correo, String clave, int estado)
    {
        this.id_usu = id_usu;
        this.correo = correo;
        this.clave = clave;
        this.estado = estado;
    }

    public int getId_usu() {
        return id_usu;
    }

    public void setId_usu(int id_usu) {
        this.id_usu = id_usu;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

     @Override
    public String toString() 
    {
        return this.correo;
    }


    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SesionDTO other = (SesionDTO) obj;
        if (this.id_usu != other.id_usu) {
            return false;
        }
        if (this.estado != other.estado) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        if (!Objects.equals(this.clave, other.clave)) {
            return false;
        }
        return true;
    }    
}
