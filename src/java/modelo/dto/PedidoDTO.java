
package modelo.dto;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author JONATHAN
 */
public class PedidoDTO 
{
    private int id_pedido;
    private int id_usu;
    private Date fecha_hora;
    private int valor_total;
    private String observaciones;
    private String estado;

    
    public PedidoDTO() 
    {
        //Constructor vacio
    }

    //Constructor con todos los atributos, excepto el id_pedido:
    public PedidoDTO(int id_usu, Date fecha_hora, int valor_total, String observaciones, String estado) 
    {
        this.id_usu = id_usu;
        this.fecha_hora = fecha_hora;
        this.valor_total = valor_total;
        this.observaciones = observaciones;
        this.estado = estado;
    }
    
     //Constructor con todos los atributos:
    public PedidoDTO(int id_pedido, int id_usu, Date fecha_hora, int valor_total, String observaciones, String estado) 
    {
        this.id_pedido = id_pedido;
        this.id_usu = id_usu;
        this.fecha_hora = fecha_hora;
        this.valor_total = valor_total;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_usu() {
        return id_usu;
    }

    public void setId_usu(int id_usu) {
        this.id_usu = id_usu;
    }

    public Date getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(Date fecha_hora) {
        this.fecha_hora = fecha_hora;
    }

    public int getValor_total() {
        return valor_total;
    }

    public void setValor_total(int valor_total) {
        this.valor_total = valor_total;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    @Override
    public String toString() 
    {
        return this.id_pedido+" "+this.id_usu+" "+this.valor_total+" "+this.estado;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PedidoDTO other = (PedidoDTO) obj;
        if (this.id_pedido != other.id_pedido) {
            return false;
        }
        if (this.id_usu != other.id_usu) {
            return false;
        }
        if (this.valor_total != other.valor_total) {
            return false;
        }
        if (!Objects.equals(this.observaciones, other.observaciones)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.fecha_hora, other.fecha_hora)) {
            return false;
        }
        return true;
    }
}
