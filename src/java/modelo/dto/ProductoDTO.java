
package modelo.dto;

import java.util.Objects;

/**
 *
 * @author JONATHAN
 */
public class ProductoDTO
{
    private int id_pro;
    private String nombre_pro;
    private String descripcion_pro;
    private String imagen;
    private int und_dis;
    private int valor_pro;
    private int iva_pro;

    public ProductoDTO() 
    {
        //Contructor Vacio
    }

    //Constructor con todos los atributos, excepto el id_pro:
    public ProductoDTO(String nombre_pro, String descripcion_pro, String imagen, int und_dis, int valor_pro, int iva_pro)
    {
        this.nombre_pro = nombre_pro;
        this.descripcion_pro = descripcion_pro;
        this.imagen = imagen;
        this.und_dis = und_dis;
        this.valor_pro = valor_pro;
        this.iva_pro = iva_pro;
    }

    //Constructor con todos los atributos:
    public ProductoDTO(int id_pro, String nombre_pro, String descripcion_pro, String imagen, int und_dis, int valor_pro, int iva_pro)
    {
        this.id_pro = id_pro;
        this.nombre_pro = nombre_pro;
        this.descripcion_pro = descripcion_pro;
        this.imagen = imagen;
        this.und_dis = und_dis;
        this.valor_pro = valor_pro;
        this.iva_pro = iva_pro;
    }

    public int getId_pro() {
        return id_pro;
    }

    public void setId_pro(int id_pro) {
        this.id_pro = id_pro;
    }

    public String getNombre_pro() {
        return nombre_pro;
    }

    public void setNombre_pro(String nombre_pro) {
        this.nombre_pro = nombre_pro;
    }

    public String getDescripcion_pro() {
        return descripcion_pro;
    }

    public void setDescripcion_pro(String descripcion_pro) {
        this.descripcion_pro = descripcion_pro;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getUnd_dis() {
        return und_dis;
    }

    public void setUnd_dis(int und_dis) {
        this.und_dis = und_dis;
    }

    public int getValor_pro() {
        return valor_pro;
    }

    public void setValor_pro(int valor_pro) {
        this.valor_pro = valor_pro;
    }

    public int getIva_pro() {
        return iva_pro;
    }

    public void setIva_pro(int iva_pro) {
        this.iva_pro = iva_pro;
    }

    
     @Override
    public String toString()
    {
        return this.id_pro + "" + this.nombre_pro;
    }

    
    @Override
    public boolean equals(Object obj) 
    {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductoDTO other = (ProductoDTO) obj;
        if (this.id_pro != other.id_pro) {
            return false;
        }
        if (this.und_dis != other.und_dis) {
            return false;
        }
        if (this.valor_pro != other.valor_pro) {
            return false;
        }
        if (this.iva_pro != other.iva_pro) {
            return false;
        }
        if (!Objects.equals(this.nombre_pro, other.nombre_pro)) {
            return false;
        }
        if (!Objects.equals(this.descripcion_pro, other.descripcion_pro)) {
            return false;
        }
        if (!Objects.equals(this.imagen, other.imagen)) {
            return false;
        }
        return true;
    }
}
