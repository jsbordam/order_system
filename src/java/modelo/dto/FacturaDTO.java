/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dto;

/**
 *
 * @author JONATHAN
 */
public class FacturaDTO 
{
    private String imagen;
    private int id_pedido;
    private int id_pro;
    private String nombre_pro;
    private int unidades_pro;
    private int valor_pro;
    private int iva_pro;
    private int total;

    public FacturaDTO() {
    }

    public FacturaDTO(String imagen, int id_pedido, int id_pro, String nombre_pro, int unidades_pro, int valor_pro, int iva_pro, int total) {
        this.imagen = imagen;
        this.id_pedido = id_pedido;
        this.id_pro = id_pro;
        this.nombre_pro = nombre_pro;
        this.unidades_pro = unidades_pro;
        this.valor_pro = valor_pro;
        this.iva_pro = iva_pro;
        this.total = total;
    }

    
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getId_pro() {
        return id_pro;
    }

    public void setId_pro(int id_pro) {
        this.id_pro = id_pro;
    }

    public String getNombre_pro() {
        return nombre_pro;
    }

    public void setNombre_pro(String nombre_pro) {
        this.nombre_pro = nombre_pro;
    }

    public int getUnidades_pro() {
        return unidades_pro;
    }

    public void setUnidades_pro(int unidades_pro) {
        this.unidades_pro = unidades_pro;
    }

    public int getValor_pro() {
        return valor_pro;
    }

    public void setValor_pro(int valor_pro) {
        this.valor_pro = valor_pro;
    }

    public int getIva_pro() {
        return iva_pro;
    }

    public void setIva_pro(int iva_pro) {
        this.iva_pro = iva_pro;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
}
