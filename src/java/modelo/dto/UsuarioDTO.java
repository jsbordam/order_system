
package modelo.dto;

import java.util.Objects;

/**
 *
 * @author JONATHAN
 */
public class UsuarioDTO 
{
    private int id_usu;
    private String nombres;
    private String apellidos;
    private String tipo_docto;
    private String num_docto;
    private String ciudad_residencia;
    private String direccion;
    private String rol;

    
    public UsuarioDTO() 
    {
        //Constructor vacio
    }

    //Constructor con todos los atributos, excepto el id_usu:
    public UsuarioDTO(String nombres, String apellidos, String tipo_docto, String num_docto, String ciudad_residencia, String direccion, String rol) 
    {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipo_docto = tipo_docto;
        this.num_docto = num_docto;
        this.ciudad_residencia = ciudad_residencia;
        this.direccion = direccion;
        this.rol = rol;
    }

    //Constructor con todos los atributos:
    public UsuarioDTO(int id_usu, String nombres, String apellidos, String tipo_docto, String num_docto, String ciudad_residencia, String direccion, String rol) 
    {
        this.id_usu = id_usu;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipo_docto = tipo_docto;
        this.num_docto = num_docto;
        this.ciudad_residencia = ciudad_residencia;
        this.direccion = direccion;
        this.rol = rol;
    }

    public int getId_usu() {
        return id_usu;
    }

    public void setId_usu(int id_usu) {
        this.id_usu = id_usu;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTipo_docto() {
        return tipo_docto;
    }

    public void setTipo_docto(String tipo_docto) {
        this.tipo_docto = tipo_docto;
    }

    public String getNum_docto() {
        return num_docto;
    }

    public void setNum_docto(String num_docto) {
        this.num_docto = num_docto;
    }

    public String getCiudad_residencia() {
        return ciudad_residencia;
    }

    public void setCiudad_residencia(String ciudad_residencia) {
        this.ciudad_residencia = ciudad_residencia;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    
    @Override
    public String toString() 
    {
        return this.nombres + " " + this.apellidos;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioDTO other = (UsuarioDTO) obj;
        if (this.id_usu != other.id_usu) {
            return false;
        }
        if (!Objects.equals(this.nombres, other.nombres)) {
            return false;
        }
        if (!Objects.equals(this.apellidos, other.apellidos)) {
            return false;
        }
        if (!Objects.equals(this.tipo_docto, other.tipo_docto)) {
            return false;
        }
        if (!Objects.equals(this.num_docto, other.num_docto)) {
            return false;
        }
        if (!Objects.equals(this.ciudad_residencia, other.ciudad_residencia)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        if (!Objects.equals(this.rol, other.rol)) {
            return false;
        }
        return true;
    }
    
    
    

}
