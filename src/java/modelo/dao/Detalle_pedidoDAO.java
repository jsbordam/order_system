
package modelo.dao;

import conexiones.ConexionMysql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.dto.Detalle_pedidoDTO;

/**
 *
 * @author JONATHAN
 */
public class Detalle_pedidoDAO 
{
    //CRUD de la base de datos:
    private static final String SQL_READ_ALL = "SELECT * FROM tb_detalle_pedido"; //Lee todos los detalle_pedidos
    
    private static final String SQL_READ = "SELECT * FROM tb_detalle_pedido WHERE id_pedido = ?"; //Lee un detalle_pedido en especifico
    
    private static final String SQL_INSERT = "INSERT INTO tb_detalle_pedido (id_pedido, id_prod, cantidad_und) VALUES (?, ?, ?)"; //Inserta detalle_pedidos
    
    private static final String SQL_DELETE = "DELETE FROM tb_detalle_pedido WHERE id_pedido = ?"; //Elimina detalle_pedidos
    
    private static final String SQL_UPDATE = "UPDATE tb_detalle_pedido SET cantidad_und = ? WHERE id_pedido = ? AND id_prod = ?"; //Actualiza detalle_pedidos
    
    private static final ConexionMysql con = ConexionMysql.getInstance();
    
    
    //Metodo para insertar un detalle_pedido:
    public boolean create (Detalle_pedidoDTO c)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setInt(1, c.getId_pedido());           
            ps.setInt(2, c.getId_prod());
            ps.setInt(3, c.getCantidad_und());
           
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al crear detalle_pedido: " + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false;
    }
    
    //Metodo para consultar todos los detalle_pedidos:
    public List <Detalle_pedidoDTO> readAll ()
    {
        List <Detalle_pedidoDTO> list = null;       
        PreparedStatement psmt;
        try 
        {
            psmt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psmt.executeQuery();
            list = new ArrayList<>();

            while (rs.next())
            {
                Detalle_pedidoDTO obj = new Detalle_pedidoDTO (
                        rs.getInt("id_pedido"), 
                        rs.getInt("id_prod"),                       
                       rs.getInt("cantidad_und"));                     
                list.add(obj);
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al mostrar detalle_pedidos: " + ex.getMessage());
        }

        finally 
        {
            con.cerrarConexion();
        }
           
        return list;
    }
    
    //Metodo para eliminar un detalle_pedido:
    public boolean delete (Detalle_pedidoDTO item)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_pedido());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al borrar detalle_pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false; 
    }
    
    //Metodo para actualizar los detalle_pedidos:
    public boolean update (Detalle_pedidoDTO item)
    {
        PreparedStatement ps;        
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setInt(1, item.getCantidad_und());           
            ps.setInt(2, item.getId_pedido());
            ps.setInt(3, item.getId_prod());

            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al editar detalle_pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }                   
        return false;
    }
    
    
    //Metodo para consultar un detalle_pedido:
    public Detalle_pedidoDTO read (Detalle_pedidoDTO filtro)
    {
        Detalle_pedidoDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_READ);
            ps.setInt(1, filtro.getId_pedido());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new Detalle_pedidoDTO (
                        rs.getInt("id_pedido"), 
                        rs.getInt("id_prod"),                       
                       rs.getInt("cantidad_und"));   
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar detalle_pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    } 
}
