
package modelo.dao;

import conexiones.ConexionMysql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.dto.PedidoDTO;

/**
 *
 * @author JONATHAN
 */
public class PedidoDAO 
{  
    //CRUD de la base de datos:
    private static final String SQL_READ_ALL = "SELECT * FROM tb_pedido"; //Lee todos los pedidos
    
    private static final String SQL_READ = "SELECT * FROM tb_pedido WHERE id_pedido = ?"; //Lee un pedido en especifico
    
    private static final String SQL_INSERT = "INSERT INTO tb_pedido (id_usu, fecha_hora, valor_total, observaciones, estado) VALUES (?, ?, ?, ?, ?)"; //Inserta pedidos
    
    private static final String SQL_DELETE = "DELETE FROM tb_pedido WHERE id_pedido = ?"; //Elimina pedidos
    
    private static final String SQL_UPDATE = "UPDATE tb_pedido SET id_usu = ?, fecha_hora = ?, valor_total = ?, observaciones = ?, estado = ? WHERE id_pedido = ?"; //Actualiza pedidos
    
    private static final ConexionMysql con = ConexionMysql.getInstance();
    
    
    //Metodo para insertar un pedidos:
    public boolean create (PedidoDTO c)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setInt(1, c.getId_usu());
            ps.setDate(2, new java.sql.Date(c.getFecha_hora().getTime()));
            ps.setInt(3, c.getValor_total());
            ps.setString(4, c.getObservaciones());
            ps.setString(5, c.getEstado());
           
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al crear pedido: " + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false;
    }
    
    //Metodo para consultar todos los pedidos:
    public List <PedidoDTO> readAll ()
    {
        List <PedidoDTO> list = null;       
        PreparedStatement psmt;
        try 
        {
            psmt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psmt.executeQuery();
            list = new ArrayList<>();

            while (rs.next())
            {
                PedidoDTO obj = new PedidoDTO (
                        rs.getInt("id_pedido"), 
                        rs.getInt("id_usu"), 
                        rs.getDate("fecha_hora"), 
                        rs.getInt("valor_total"),
                        rs.getString("observaciones"), 
                        rs.getString("estado"));
                list.add(obj);
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al mostrar pedidos: " + ex.getMessage());
        }

        finally 
        {
            con.cerrarConexion();
        }
           
        return list;
    }
    
    //Metodo para eliminar un pedidos:
    public boolean delete (PedidoDTO item)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_pedido());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al borrar pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false; 
    }
    
    //Metodo para actualizar los pedidos:
    public boolean update (PedidoDTO item)
    {
        PreparedStatement ps;        
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setInt(1, item.getId_usu());
            ps.setDate(2, new java.sql.Date(item.getFecha_hora().getTime()));
            ps.setInt(3, item.getValor_total());
            ps.setString(4, item.getObservaciones());
            ps.setString(5, item.getEstado());
            ps.setInt(6, item.getId_pedido());

            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al editar pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }                   
        return false;
    }
    
    
    //Metodo para consultar un pedidos:
    public PedidoDTO read (PedidoDTO filtro)
    {
        PedidoDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_READ);
            ps.setInt(1, filtro.getId_pedido());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new PedidoDTO (
                        rs.getInt("id_pedido"), 
                        rs.getInt("id_usu"), 
                        rs.getDate("fecha_hora"), 
                        rs.getInt("valor_total"),
                        rs.getString("observaciones"), 
                        rs.getString("estado"));        
            }             
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar pedido..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    } 
}
