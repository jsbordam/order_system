
package modelo.dao;

import conexiones.ConexionMysql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class UsuarioDAO 
{
    //CRUD de la base de datos:
    private static final String SQL_READ_ALL = "SELECT * FROM tb_usuario"; //Lee todos los usuarios
    
    private static final String SQL_READ = "SELECT * FROM tb_usuario WHERE id_usu = ?"; //Lee un usuario en especifico
    
    private static final String SQL_INSERT = "INSERT INTO tb_usuario (nombres, apellidos, tipo_docto, num_docto, ciudad_residencia, direccion, rol) VALUES (?, ?, ?, ?, ?, ?, ?)"; //Inserta usuarios
    
    private static final String SQL_DELETE = "DELETE FROM tb_usuario WHERE id_usu = ?"; //Elimina usuarios
    
    private static final String SQL_UPDATE = "UPDATE tb_usuario SET nombres = ?, apellidos = ?, tipo_docto = ?, num_docto = ?, ciudad_residencia = ?, direccion = ?, rol = ? WHERE id_usu = ?"; //Actualiza usuarios
        
    private static final ConexionMysql con = ConexionMysql.getInstance();
    
    
     //Metodo para insertar un producto:
    public boolean create (UsuarioDTO c)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setString(1, c.getNombres());
            ps.setString(2, c.getApellidos());
            ps.setString(3, c.getTipo_docto());
            ps.setString(4, c.getNum_docto());
            ps.setString(5, c.getCiudad_residencia());
            ps.setString(6, c.getDireccion());
            ps.setString(7, c.getRol());      
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al crear usuario: " + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false;
    }
    
    
    //Metodo para consultar todos los productos:
    public List <UsuarioDTO> readAll ()
    {
        List <UsuarioDTO> list = null;       
        PreparedStatement psmt;
        try 
        {
            psmt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psmt.executeQuery();
            list = new ArrayList<>();

            while (rs.next())
            {
                UsuarioDTO obj = new UsuarioDTO (rs.getInt("id_usu"), rs.getString("nombres"), rs.getString("apellidos"), rs.getString("tipo_docto"), rs.getString("num_docto"), rs.getString("ciudad_residencia"), rs.getString("direccion"), rs.getString("rol"));
                list.add(obj);
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al mostrar usuarios: " + ex.getMessage());
        }

        finally 
        {
            con.cerrarConexion();
        }
           
        return list;
    }
    
    
    //Metodo para eliminar un producto:
    public boolean delete (UsuarioDTO item)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_usu());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al borrar usuario..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false; 
    }
    
    //Metodo para actualizar los productos:
    public boolean update (UsuarioDTO item)
    {
        PreparedStatement ps;
        
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getNombres());
            ps.setString(2, item.getApellidos());
            ps.setString(3, item.getTipo_docto());
            ps.setString(4, item.getNum_docto());
            ps.setString(5, item.getCiudad_residencia());
            ps.setString(6, item.getDireccion());
            ps.setString(7, item.getRol());      
            ps.setInt(8, item.getId_usu());

            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al actualizar usuario..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        } 
                     
        return false;
    }
    
    
     //Metodo para consultar un producto:
    public UsuarioDTO read (UsuarioDTO filtro)
    {
        UsuarioDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_READ);
            ps.setInt(1, filtro.getId_usu());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new UsuarioDTO (rs.getInt("id_usu"), rs.getString("nombres"), rs.getString("apellidos"), rs.getString("tipo_docto"), rs.getString("num_docto"), rs.getString("ciudad_residencia"), rs.getString("direccion"), rs.getString("rol"));           }             
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar usuario..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    }
   
}
