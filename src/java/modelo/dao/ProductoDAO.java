
package modelo.dao;

import conexiones.ConexionMysql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.dto.ProductoDTO;

/**
 *
 * @author JONATHAN
 */
public class ProductoDAO 
{
    //CRUD de la base de datos:
    private static final String SQL_READ_ALL = "SELECT * FROM tb_producto"; //Lee todos los productos
    
    private static final String SQL_READ = "SELECT * FROM tb_producto WHERE id_pro = ?"; //Lee un producto en especifico
    
    private static final String SQL_INSERT = "INSERT INTO tb_producto (nombre_pro, descripcion_pro, imagen, und_dis, valor_pro, iva_pro) VALUES (?, ?, ?, ?, ?, ?)"; //Inserta productos
    
    private static final String SQL_DELETE = "DELETE FROM tb_producto WHERE id_pro = ?"; //Elimina productos
    
    private static final String SQL_UPDATE = "UPDATE tb_producto SET nombre_pro = ?, descripcion_pro = ?, imagen = ?, und_dis = ?, valor_pro = ?, iva_pro = ? WHERE id_pro = ?"; //Actualiza productos
    
    private static final ConexionMysql con = ConexionMysql.getInstance();
    
    
    //Metodo para insertar un producto:
    public boolean create (ProductoDTO c)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setString(1, c.getNombre_pro());
            ps.setString(2, c.getDescripcion_pro());
            ps.setString(3, c.getImagen());
            ps.setInt(4, c.getUnd_dis());
            ps.setInt(5, c.getValor_pro());
            ps.setInt(6, c.getIva_pro());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al crear producto: " + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false;
    }
    
    //Metodo para consultar todos los productos:
    public List <ProductoDTO> readAll ()
    {
        List <ProductoDTO> list = null;       
        PreparedStatement psmt;
        try 
        {
            psmt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psmt.executeQuery();
            list = new ArrayList<>();

            while (rs.next())
            {
                ProductoDTO obj = new ProductoDTO (
                        rs.getInt("id_pro"), 
                        rs.getString("nombre_pro"), 
                        rs.getString("descripcion_pro"), 
                        rs.getString("imagen"), 
                        rs.getInt("und_dis"), 
                        rs.getInt("valor_pro"), 
                        rs.getInt("iva_pro"));
                list.add(obj);
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al mostrar productos: " + ex.getMessage());
        }

        finally 
        {
            con.cerrarConexion();
        }
           
        return list;
    }
    
    //Metodo para eliminar un producto:
    public boolean delete (ProductoDTO item)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_pro());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al borrar producto..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false; 
    }
    
    //Metodo para actualizar los productos:
    public boolean update (ProductoDTO item)
    {
        PreparedStatement ps;        
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getNombre_pro());
            ps.setString(2, item.getDescripcion_pro());
            ps.setString(3, item.getImagen());
            ps.setInt(4, item.getUnd_dis());
            ps.setInt(5, item.getValor_pro());
            ps.setInt(6, item.getIva_pro());
            ps.setInt(7, item.getId_pro());

            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al editar producto..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }                   
        return false;
    }
    
    
    //Metodo para consultar un producto:
    public ProductoDTO read (ProductoDTO filtro)
    {
        ProductoDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_READ);
            ps.setInt(1, filtro.getId_pro());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new ProductoDTO (rs.getInt("id_pro"), rs.getString("nombre_pro"), rs.getString("descripcion_pro"), rs.getString("imagen"), rs.getInt("und_dis"), rs.getInt("valor_pro"), rs.getInt("iva_pro"));            }             
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar producto..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    } 
}
