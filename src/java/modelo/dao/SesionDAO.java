
package modelo.dao;

import conexiones.ConexionMysql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.dto.SesionDTO;

/**
 *
 * @author JONATHAN
 */
public class SesionDAO
{
    //CRUD de la base de datos:
    private static final String SQL_READ_ALL = "SELECT * FROM tb_sesion_app"; //Lee todos las sesiones
    
    private static final String SQL_READ = "SELECT * FROM tb_sesion_app WHERE id_usu = ?"; //Lee una sesion en especifico
    
    private static final String SQL_INSERT = "INSERT INTO tb_sesion_app (id_usu, correo, clave, estado) VALUES (?, ?, ?, ?)"; //Inserta sesiones
    
    private static final String SQL_DELETE = "DELETE FROM tb_sesion_app WHERE id_usu = ?"; //Elimina sesiones
    
    private static final String SQL_UPDATE = "UPDATE tb_sesion_app SET  correo = clave = ?, estado = ? WHERE id_usu = ?"; //Actualiza sesiones

    private static final String SQL_LOGIN = "SELECT * FROM tb_sesion_app WHERE correo = ? AND clave = ?";  //Valida la sesion con correo y contraseña
    
    private static final ConexionMysql con = ConexionMysql.getInstance();
    
    
    //Metodo para insertar una sesion:
    public boolean create (SesionDTO c)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_INSERT);
            ps.setInt(1, c.getId_usu());      
            ps.setString(2, c.getCorreo());
            ps.setString(3, c.getClave());
            ps.setInt(4, c.getEstado());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al crear sesion: " + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false;
    }
    
    //Metodo para consultar todos las sesiones:
    public List <SesionDTO> readAll ()
    {
        List <SesionDTO> list = null;       
        PreparedStatement psmt;
        try 
        {
            psmt = con.getCnn().prepareStatement(SQL_READ_ALL);
            ResultSet rs = psmt.executeQuery();
            list = new ArrayList<>();

            while (rs.next())
            {
                SesionDTO obj = new SesionDTO (             
                        rs.getInt("id_usu"), 
                        rs.getString("correo"), 
                        rs.getString("clave"),
                        rs.getInt("estado"));                     
                list.add(obj);
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al mostrar sesiones: " + ex.getMessage());
        }

        finally 
        {
            con.cerrarConexion();
        }
           
        return list;
    }
    
    //Metodo para eliminar una sesion:
    public boolean delete (SesionDTO item)
    {
        PreparedStatement ps;
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_DELETE);
            ps.setInt(1, item.getId_usu());
            
            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al borrar sesion..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        return false; 
    }
    
    //Metodo para actualizar las sesiones:
    public boolean update (SesionDTO item)
    {
        PreparedStatement ps;        
        try 
        {
            ps = con.getCnn().prepareStatement(SQL_UPDATE);
            ps.setString(1, item.getCorreo());
            ps.setString(2, item.getClave());
            ps.setInt(3, item.getEstado());
            ps.setInt(4, item.getId_usu());

            if (ps.executeUpdate() > 0)
            {
                return true;
            }
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al editar sesion..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }                   
        return false;
    }
    
    
    //Metodo para consultar una sesion:
    public SesionDTO read (SesionDTO filtro)
    {
        SesionDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_READ);
            ps.setInt(1, filtro.getId_usu());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new SesionDTO (
                        rs.getInt("id_usu"), 
                        rs.getString("correo"), 
                        rs.getString("clave"),
                        rs.getInt("estado"));         
            }             
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar sesion..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    } 
    
    //Metodo para validar la sesion:
    public SesionDTO login (SesionDTO usuario)
    {
        SesionDTO objres = null;
        
        PreparedStatement ps;

        try 
        {
            ps = con.getCnn().prepareStatement(SQL_LOGIN);
            ps.setString(1, usuario.getCorreo());
            ps.setString (2, usuario.getClave());
            ResultSet rs = ps.executeQuery();

            if (rs.next())
            {
                objres = new SesionDTO (
                          rs.getInt("id_usu"), 
                        rs.getString("correo"), 
                        rs.getString("clave"),
                        rs.getInt("estado"));        
            }             
        } 
        catch (SQLException ex) 
        {
            System.out.println ("Error al mostrar sesion..." + ex.getMessage());
        }
        finally 
        {
            con.cerrarConexion();
        }
        
        return objres;
    }
}
