/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class CuentaAdmin2CTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
        String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 

        if (menu.equalsIgnoreCase("Usuario")) 
        {
            Facade obj = new Facade();
            switch (accion) 
            {
                //Para mostrar todos los usuarios:
                case "Listar":
                    List<UsuarioDTO> lis = obj.listarUsuarios();
                    request.setAttribute("Lista_usuarios", lis);
                    //Listo las sesiones tambien:
                    List<SesionDTO> lis2 = obj.listarSesiones();
                    request.setAttribute("Lista_sesiones", lis2);
                break;

                //Para insertar un usuario:
                case "Agregar":
                    String nombres = request.getParameter("txt_nombres");
                    String apellidos = request.getParameter("txt_apellidos");
                    String tipo_docto = request.getParameter("txt_tipo_docto");
                    String num_docto = request.getParameter("txt_num_docto");
                    String ciudad_residencia = request.getParameter("txt_ciudad_residencia");
                    String direccion = request.getParameter("txt_direccion");
                    String rol = request.getParameter("txt_rol");
                    String correo = request.getParameter("txt_correo");
                    String clave = request.getParameter("txt_clave");
                    int estado = Integer.parseInt(request.getParameter("txt_estado"));
                    
                    //Agrega un nuevo usuario:
                    UsuarioDTO nuevo = new UsuarioDTO(nombres, apellidos, tipo_docto, num_docto, ciudad_residencia, direccion, rol);
                    obj.crearUsuario(nuevo);       
                    
                    //Lista el nuevo usuario:
                    List<UsuarioDTO> lisU = obj.listarUsuarios();
                    
                    //Busca el id de ese usuario que acaba de llegar, para asociarle una sesion:
                    int id=0;
                    for (int i=0; i<lisU.size(); i++)
                    {
                        if (lisU.get(i).getNum_docto().equals(num_docto))
                        {
                            id = lisU.get(i).getId_usu();
                        }
                    }
                    
                    //Concateno el usuario con una sesion:
                    SesionDTO nueva = new SesionDTO (id, correo, clave, estado);
                    obj.crearSesion(nueva);           
                    request.getRequestDispatcher("CuentaAdmin2CTO?menu=Usuario&accion=Listar").forward(request, response);
                    break;

                //Para editar un usuario:
                case "Editar":
                    UsuarioDTO edit = new UsuarioDTO();
                    edit.setId_usu(Integer.parseInt(request.getParameter("id")));
                    edit = obj.verUsuario(edit);
                    request.setAttribute("usuario", edit);                   
                    request.getRequestDispatcher("CuentaAdmin2CTO?menu=Usuario&accion=Listar").forward(request, response);
                break;

                //Para actualizar un usuario:
                case "Actualizar":
                    int Id = Integer.parseInt(request.getParameter("txt_id_usu"));
                    String nombres2 = request.getParameter("txt_nombres");
                    String apellidos2 = request.getParameter("txt_apellidos");
                    String tipo_docto2 = request.getParameter("txt_tipo_docto");
                    String num_docto2 = request.getParameter("txt_num_docto");
                    String ciudad_residencia2 = request.getParameter("txt_ciudad_residencia");
                    String direccion2 = request.getParameter("txt_direccion");
                    String rol2 = request.getParameter("txt_rol");
                    String correo2 = request.getParameter("txt_correo");
                    String clave2 = request.getParameter("txt_clave");
                    int estado2 = Integer.parseInt(request.getParameter("txt_estado"));
                    
                    //Agrega un nuevo usuario:
                    UsuarioDTO actualizar = new UsuarioDTO(Id, nombres2, apellidos2, tipo_docto2, num_docto2, ciudad_residencia2, direccion2, rol2);
                    obj.actualizarUsuario(actualizar);                                                            
                    request.getRequestDispatcher("CuentaAdmin2CTO?menu=Usuario&accion=Listar").forward(request, response);
                    break;

                //Para eliminar un usuario:
                case "Eliminar":
                    UsuarioDTO elim = new UsuarioDTO();
                    elim.setId_usu(Integer.parseInt(request.getParameter("id")));
                    obj.eliminarUsuario(elim);               
                    request.getRequestDispatcher("CuentaAdmin2CTO?menu=Usuario&accion=Listar").forward(request, response);
                    break;

            }
            request.getRequestDispatcher("AdminUsuario.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
