/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class CuentaClienteCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
         String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
         String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
                          
         if (menu.equalsIgnoreCase("Cuenta"))
         {
             Facade obj = new Facade();
             switch (accion)
             {
                 //Para mostrar todos los usuarios:
                 case "Listar":                                      
                    //Primero obtengo el id del usuario que esta en la sesion:
                    HttpSession objLogin = request.getSession(); //Se obtiene el objeto de la sesion
                    SesionDTO usuRegistrado = (SesionDTO) objLogin.getAttribute("login"); //Se obtiene el usuario que entra a la sesion                    
                                      
                    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
                    List<SesionDTO> lisS = obj.listarSesiones();
                    int id_usu=0;

                    for (int i=0; i<lisS.size(); i++)
                    {
                        if (lisS.get(i).getCorreo().equals(usuRegistrado.getCorreo()))
                        {
                            //Id del usuario que esta en la sesion
                            id_usu = lisS.get(i).getId_usu();
                        }                   
                    }
                    
                    //Como solo quiero mostrar un usuario entonces lo guardo en 
                    //una nueva lista:
                    List <UsuarioDTO> listUsu = null; 
                    listUsu = new ArrayList<>();
                    
                    //Me trae la lista de los usuarios: 
                    List<UsuarioDTO> lisU = obj.listarUsuarios();
                    
                    //Busco en la lista de usuarios el id encontrado anteriormente:
                    for (int i=0; i<lisU.size(); i++)
                    {
                        if (lisU.get(i).getId_usu() == id_usu)
                        {
                            //Ya tengo el usuario que esta en la sesion:
                            String nombres = lisU.get(i).getNombres();
                            String apellidos = lisU.get(i).getApellidos();
                            String tipo_docto = lisU.get(i).getTipo_docto();
                            String num_docto = lisU.get(i).getNum_docto();
                            String ciudad = lisU.get(i).getCiudad_residencia();
                            String direccion = lisU.get(i).getDireccion();
                            String rol = lisU.get(i).getRol();
                            UsuarioDTO usu = new UsuarioDTO (id_usu, nombres, apellidos, tipo_docto, num_docto, ciudad, direccion, rol);
                            listUsu.add(usu); //Guardo el usuario en la nueva lista    
                        }                   
                    }
                    
                    //Por ultimo envio la lista para que se muestre:
                    request.setAttribute("Lista_usuario", listUsu);
                 break;
                                               
                 //Para editar un usuario:
                 case "Editar":
                     UsuarioDTO edit = new UsuarioDTO();
                     edit.setId_usu(Integer.parseInt(request.getParameter("id"))); 
                     edit = obj.verUsuario(edit);
                     request.setAttribute("usuario", edit);
                     request.getRequestDispatcher("CuentaClienteCTO?menu=Cuenta&accion=Listar").forward(request,response);
                 break;
                 
                 //Para actualizar un usuario:
                 case "Actualizar": 
                    int id_usuario = Integer.parseInt(request.getParameter("txt_id_usu"));
                    String nombres = request.getParameter("txt_nombres");
                    String apellidos = request.getParameter("txt_apellidos");
                    String tipo_docto = request.getParameter("txt_tipo_docto");
                    String num_docto = request.getParameter("txt_num_docto");
                    String ciudad_residencia = request.getParameter("txt_ciudad_residencia");
                    String direccion = request.getParameter("txt_direccion");
                    String rol = request.getParameter("txt_rol");                              
                     
                    //Actualiza el nuevo usuario:
                    UsuarioDTO actualizar = new UsuarioDTO(id_usuario, nombres, apellidos, tipo_docto, num_docto, ciudad_residencia, direccion, rol);
                    obj.actualizarUsuario(actualizar); 
                    
                    //Lista el nuevo usuario:
                    List<UsuarioDTO> lis = obj.listarUsuarios();
                    request.getRequestDispatcher("CuentaClienteCTO?menu=Cuenta&accion=Listar").forward(request,response);                           
                 break;               
             }
             request.getRequestDispatcher("CuentaCliente.jsp").forward(request,response);
         }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
