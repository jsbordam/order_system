/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.dto.Detalle_pedidoDTO;
import modelo.dto.FacturaDTO;
import modelo.dto.PedidoDTO;
import modelo.dto.ProductoDTO;
import modelo.dto.SesionDTO;

/**
 *
 * @author JONATHAN
 */
public class FacCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    int total;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
         String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
         
         
         if (menu.equalsIgnoreCase("Factura"))
         {
             Facade obj = new Facade();
             switch (accion)
             {            
                 //Para generar la lista de informacion de la factura final:
                 case "Generar":
                    //Declaro la lista de factura:
                    List <FacturaDTO> listFac = null; 
                    listFac = new ArrayList<>();
                    
                    //Captura las listas que requiero para los datos:
                    
                    //Lista de pedidos:
                    List<PedidoDTO> lisPe = obj.listarPedidos(); 
                    
                    //Lista de productos:
                    List<ProductoDTO> lisPro = obj.listarProductos();
                    
                    //Lista de detalle_pedido
                    List<Detalle_pedidoDTO> lisDetalle = obj.listarDetalle_pedidos();
                    
                    //////////////////////////////////////////////////////////////////////
                    //Campos de la lista Factura:
                    String imagen = "";
                    int id_pedido = 0; 
                    int id_pro = 0;
                    int unidades_pro = 0;  
                    String nombre_pro = "";
                    int valor_pro = 0;
                    int iva_pro = 0;
                    total = 0;
                    int totalPro = 0;
                    //////////////////////////////////////////////////////////////////////
                    //Obtengo el id del usuario que esta en la sesion:
                    HttpSession objLogin = request.getSession(); //Se obtiene el objeto de la sesion
                    SesionDTO usuRegistrado = (SesionDTO) objLogin.getAttribute("login"); //Se obtiene el usuario que entra a la sesion                                   
                    
                
                    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
                    List<SesionDTO> lisS = obj.listarSesiones();
                    int id_usu=0;

                    for (int j=0; j<lisS.size(); j++)
                    {
                        if (lisS.get(j).getCorreo().equals(usuRegistrado.getCorreo()))
                        {
                            //Rescato el id que tiene registrado ese correo en sesiones:
                            id_usu = lisS.get(j).getId_usu();
                        }                   
                    }
                    
                    String estado = "";
                    //Guardo el id del pedido de ese usuario:
                    for (int i=0; i<lisPe.size(); i++)
                    {
                        //Encuentra en los pedidos el id del usuario:
                        if (lisPe.get(i).getId_usu() == id_usu)
                        {
                            id_pedido = lisPe.get(i).getId_pedido();
                            estado = lisPe.get(i).getEstado();
                        }
                    }
                    
                    ///////////////////////////////////////////////////
                    //Para generar la factura revisa el estado del pedido:
                    if (estado.equals("Seleccionado"))
                    {
                        //Si el estado es seleccionado quiere decir que aun 
                        //esta seleccionando productos en el carrito
                        
                        //Guardo el id de cada producto registrado a ese pedido:             
                        for (int i=0; i<lisDetalle.size(); i++)
                        {
                            //Encuentra en los detalle_pedido, el producto registrado al pedido:
                            if (lisDetalle.get(i).getId_pedido() == id_pedido)
                            {
                                id_pro = lisDetalle.get(i).getId_prod();
                                unidades_pro = lisDetalle.get(i).getCantidad_und();

                                //Busco el producto con el id en la lista de productos:
                                for (int j=0; j<lisPro.size(); j++)
                                {
                                    //Encuentra el producto y capturo datos que nesecito:
                                    if (lisPro.get(j).getId_pro() == id_pro)
                                    {
                                        imagen = lisPro.get(j).getImagen();
                                        nombre_pro = lisPro.get(j).getNombre_pro();
                                        valor_pro = lisPro.get(j).getValor_pro();
                                        total  = total + (valor_pro * unidades_pro); //Acumulo el total de los productos
                                        iva_pro = lisPro.get(j).getIva_pro();
                                        totalPro = valor_pro * unidades_pro;
                                    }                               
                                }

                                //Lleno la lista de la factura ya que tengo todo:

                                //Creo un objeto de factura:
                                FacturaDTO fac = new FacturaDTO(imagen, id_pedido, id_pro, nombre_pro, unidades_pro, valor_pro, iva_pro, totalPro);
                                //La agrego a la lista:
                                listFac.add(fac);                   
                            }                     
                        }
                        //Luego de tener la factura total, la envio al jsp de Factura:
                        request.setAttribute("Lista_factura", listFac);
                        request.setAttribute("Total_neto", total);
                    }
                    else
                    {
                        //Ya no tiene pedidos activos
                        System.out.println("Ya esta solicitado");
                        request.getRequestDispatcher("PedidoSolicitado.jsp").forward(request, response);
                    }
                    request.getRequestDispatcher("FacturaVTA.jsp").forward(request,response);                        
                 break;
                 
                 //Para actualizar el estado del pedido:
                 case "Actualizar":
                    //Obtengo el id del usuario que esta en la sesion:
                    HttpSession objLogin2 = request.getSession(); //Se obtiene el objeto de la sesion
                    SesionDTO usuRegistrado2 = (SesionDTO) objLogin2.getAttribute("login"); //Se obtiene el usuario que entra a la sesion                    
                               
                    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
                    List<SesionDTO> lisS2 = obj.listarSesiones();
                    int id_us=0;

                    for (int i=0; i<lisS2.size(); i++)
                    {
                        if (lisS2.get(i).getCorreo().equals(usuRegistrado2.getCorreo()))
                        {
                            //Rescato el id que tiene registrado ese correo en sesiones:
                            id_us = lisS2.get(i).getId_usu();
                        }                   
                    }
                    
                    //Lista de pedidos:
                    List<PedidoDTO> lisPe2 = obj.listarPedidos(); 
                    //Guardo el id del pedido de ese usuario:
                    id_pedido = 0;
                    //Ya tengo id_us
                    java.util.Date fecha = null;
                    //Valor total es "total" variable global
                    String obser = "";
                    String est = "Solicitado";
                    
                    for (int i=0; i<lisPe2.size(); i++)
                    {
                        //Encuentra en los pedidos el id del usuario:
                        if (lisPe2.get(i).getId_usu() == id_us && lisPe2.get(i).getEstado().equals("Seleccionado")) 
                        {
                            id_pedido = lisPe2.get(i).getId_pedido();
                            fecha = lisPe2.get(i).getFecha_hora();
                            obser = lisPe2.get(i).getObservaciones();
                        }
                    }
                    
                    //Actualizamos el estado de ese pedido:                   
                    PedidoDTO actual = new PedidoDTO (id_pedido, id_us, fecha, total, obser, est);
                    obj.actualizarPedido(actual);                                     
                    request.getRequestDispatcher("FacCTO?menu=Factura&accion=Generar").forward(request,response);
                break;                               
             }
         }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
