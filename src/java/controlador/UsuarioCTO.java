
package controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class UsuarioCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    { 
        String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
        String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
        
        if (menu.equalsIgnoreCase("Usuario")) 
        {
            Facade obj = new Facade();
            switch (accion) 
            {     
                //Para insertar un usuario:
                case "Registrarse":
                    String nombres = request.getParameter("txt_nombres");
                    String apellidos = request.getParameter("txt_apellidos");
                    String tipo_docto = request.getParameter("txt_tipo_docto");
                    String num_docto = request.getParameter("txt_num_docto");
                    String ciudad_residencia = request.getParameter("txt_ciudad_residencia");
                    String direccion = request.getParameter("txt_direccion");
                    String rol = request.getParameter("txt_rol");
                    String correo = request.getParameter("txt_correo");
                    String clave = request.getParameter("txt_clave");
                    int estado = Integer.parseInt(request.getParameter("txt_estado"));
                    
                    //Agrega un nuevo usuario:
                    UsuarioDTO nuevo = new UsuarioDTO(nombres, apellidos, tipo_docto, num_docto, ciudad_residencia, direccion, rol);
                    obj.crearUsuario(nuevo);
                    
                    //Lista el nuevo usuario:
                    List<UsuarioDTO> lis = obj.listarUsuarios();
                    
                    //Busca el id de ese usuario que acaba de llegar, para asociarle una sesion:
                    int id=0;
                    for (int i=0; i<lis.size(); i++)
                    {
                        if (lis.get(i).getNum_docto().equals(num_docto))
                        {
                            id = lis.get(i).getId_usu(); //Id del usuario
                        }
                    }
                    
                    //Concateno el usuario con una sesion:
                    SesionDTO nueva = new SesionDTO (id, correo, clave, estado);
                    obj.crearSesion(nueva);              
                break;              
            }
            request.getRequestDispatcher("LoginVTA.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
