/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.dto.ProductoDTO;

/**
 *
 * @author JONATHAN
 */
public class CuentaAdminCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
         String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
         String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
                          
         if (menu.equalsIgnoreCase("Producto"))
         {
             Facade obj = new Facade();
             switch (accion)
             {
                 //Para mostrar todos los productos:
                 case "Listar":
                    List<ProductoDTO> lis = obj.listarProductos();
                    request.setAttribute("Lista_productos", lis);
                 break;
                 
                 //Para insertar un producto:
                 case "Agregar":
                    String nombre = request.getParameter("txt_nombre_pro");
                    String des = request.getParameter("txt_descripcion_pro");
                    String imagen = request.getParameter("txt_imagen");
                    int und = Integer.parseInt(request.getParameter("txt_und_dis"));
                    int val = Integer.parseInt(request.getParameter("txt_valor_pro"));
                    int iva = Integer.parseInt(request.getParameter("txt_iva_pro"));
                    ProductoDTO nuevo = new ProductoDTO(nombre, des, imagen, und, val, iva);
                    obj.crearProducto(nuevo);
                    request.getRequestDispatcher("CuentaAdminCTO?menu=Producto&accion=Listar").forward(request,response); //Redirecciona al controlador
                 break;
                 
                 //Para editar un producto:
                 case "Editar":
                     ProductoDTO edit = new ProductoDTO();
                     edit.setId_pro(Integer.parseInt(request.getParameter("id"))); 
                     edit = obj.verProducto(edit);
                     request.setAttribute("producto", edit);
                     request.getRequestDispatcher("CuentaAdminCTO?menu=Producto&accion=Listar").forward(request,response); //Redirecciona al controlador
                 break;
                 
                 //Para actualizar un producto:
                 case "Actualizar":
                     int id = Integer.parseInt(request.getParameter("txt_id_pro"));
                     String nombre2 = request.getParameter("txt_nombre_pro");
                     String des2 = request.getParameter("txt_descripcion_pro");
                     String imagen2 = request.getParameter("txt_imagen");
                     int und2 = Integer.parseInt(request.getParameter("txt_und_dis"));
                     int val2 = Integer.parseInt(request.getParameter("txt_valor_pro"));
                     int iva2 = Integer.parseInt(request.getParameter("txt_iva_pro"));
                     ProductoDTO actual = new ProductoDTO (id, nombre2, des2, imagen2, und2, val2, iva2);
                     obj.actualizarProducto(actual);
                     request.getRequestDispatcher("CuentaAdminCTO?menu=Producto&accion=Listar").forward(request,response); //Redirecciona al controlador
                 break; 
                 
                 //Para eliminar un producto:
                 case "Eliminar":
                     ProductoDTO elim = new ProductoDTO();
                     elim.setId_pro(Integer.parseInt(request.getParameter("id")));
                     obj.eliminarProducto(elim); 
                     request.getRequestDispatcher("CuentaAdminCTO?menu=Producto&accion=Listar").forward(request,response); //Redirecciona al controlador
                 break;    
            }
            request.getRequestDispatcher("AdminProducto.jsp").forward(request,response);
         }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
