
package controlador;

import java.util.List;
import modelo.dao.Detalle_pedidoDAO;
import modelo.dao.PedidoDAO;
import modelo.dao.ProductoDAO;
import modelo.dao.SesionDAO;
import modelo.dao.UsuarioDAO;
import modelo.dto.Detalle_pedidoDTO;
import modelo.dto.PedidoDTO;
import modelo.dto.ProductoDTO;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class Facade 
{
    //Para Tabla Producto:
    
    //Metodo para crear un producto:
    public boolean crearProducto (ProductoDTO objP)
    {
        boolean rta = false;
        
        if (objP != null)
        {
            ProductoDAO dao = new ProductoDAO ();
            dao.create(objP);
        }
        return rta;
    }
    
    //Metodo para mostrar todos los productos:
    public List<ProductoDTO> listarProductos()
    {
        List<ProductoDTO> list = null;
        ProductoDAO dao = new ProductoDAO();
        list = dao.readAll();
        return list;
    }
    
    //Metodo para mostrar un producto:
    public ProductoDTO verProducto (ProductoDTO filtro)
    {
        ProductoDTO obj;
        ProductoDAO dao = new ProductoDAO();
        
        obj = dao.read(filtro);
        return obj;
    }

    
    //Metodo para actualizar un producto:
    public boolean actualizarProducto (ProductoDTO item)
    {
        boolean rta = false;
        
        if (item != null)
        {
            ProductoDAO dao = new ProductoDAO();
            rta = dao.update(item);
        }
        return rta;
    }
    
    //Metodo para eliminar un producto:
    public void eliminarProducto (ProductoDTO item)
    {
        ProductoDAO dao = new ProductoDAO();
        dao.delete(item);
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Para Tabla Usuario:
    
    //Metodo para crear un usuario:
    public boolean crearUsuario (UsuarioDTO objP)
    {
        boolean rta = false;
        
        if (objP != null)
        {
            UsuarioDAO dao = new UsuarioDAO ();
            dao.create(objP);
        }
        return rta;
    }
       
    //Metodo para mostrar todos los usuarios:
    public List<UsuarioDTO> listarUsuarios()
    {
        List<UsuarioDTO> list = null;
        UsuarioDAO dao = new UsuarioDAO();
        list = dao.readAll();
        return list;
    }
    
    //Metodo para mostrar un usuario:
    public UsuarioDTO verUsuario (UsuarioDTO filtro)
    {
        UsuarioDTO obj;
        UsuarioDAO dao = new UsuarioDAO();
        
        obj = dao.read(filtro);
        return obj;
    }
    
    //Metodo para actualizar un producto:
    public boolean actualizarUsuario (UsuarioDTO item)
    {
        boolean rta = false;
        
        if (item != null)
        {
            UsuarioDAO dao = new UsuarioDAO();
            rta = dao.update(item);
        }
        return rta;
    }
    
    //Metodo para eliminar un usuario:
    public void eliminarUsuario (UsuarioDTO item)
    {
        UsuarioDAO dao = new UsuarioDAO();
        dao.delete(item); 
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Para Tabla Sesion:
    
    //Metodo para validar la sesion:
    public SesionDTO valida_credenciales (SesionDTO usuario)
    {
        SesionDAO dao = new SesionDAO ();
        SesionDTO dto;
        
        dto = dao.login(usuario); //Me trae el usuario
        return dto;
    }
    
    //Metodo para crear una sesion:
    public boolean crearSesion (SesionDTO objP)
    {
        boolean rta = false;
        
        if (objP != null)
        {
            SesionDAO dao = new SesionDAO ();
            dao.create(objP);
        }
        return rta;
    }
    
    //Metodo para mostrar todos las sesiones:
    public List<SesionDTO> listarSesiones()
    {
        List<SesionDTO> list = null;
        SesionDAO dao = new SesionDAO();
        list = dao.readAll();
        return list;
    }
    
    //Metodo para actualizar una sesion:
    public boolean actualizarSesion (SesionDTO item)
    {
        boolean rta = false;
        
        if (item != null)
        {
            SesionDAO dao = new SesionDAO();
            rta = dao.update(item);
        }
        return rta;
    }
    
    //Metodo para mostrar una sesion:
    public SesionDTO verSesion (SesionDTO filtro)
    {
        SesionDTO obj;
        SesionDAO dao = new SesionDAO();
        
        obj = dao.read(filtro);
        return obj;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Para Tabla Pedido:
    
    //Metodo para crear un pedido:
    public boolean crearPedido (PedidoDTO objP)
    {
        boolean rta = false;
        
        if (objP != null)
        {
            PedidoDAO dao = new PedidoDAO ();
            dao.create(objP);
        }
        return rta;
    }
    
    //Metodo para mostrar todos los pedidos:
    public List<PedidoDTO> listarPedidos()
    {
        List<PedidoDTO> list = null;
        PedidoDAO dao = new PedidoDAO();
        list = dao.readAll();
        return list;
    }
    
     //Metodo para actualizar un pedido:
    public boolean actualizarPedido (PedidoDTO item)
    {
        boolean rta = false;
        
        if (item != null)
        {
            PedidoDAO dao = new PedidoDAO();
            rta = dao.update(item);
        }
        return rta;
    }
    
    //Metodo para eliminar un pedido:
    public void eliminarPedido (PedidoDTO item)
    {
        PedidoDAO dao = new PedidoDAO();
        dao.delete(item);
    }
     ////////////////////////////////////////////////////////////////////////////////////////////
    //Para Tabla Detalle_pedido:
    
    //Metodo para crear un detalle_pedido:
    public boolean crearDetalle_pedido (Detalle_pedidoDTO objP)
    {
        boolean rta = false;
        
        if (objP != null)
        {
            Detalle_pedidoDAO dao = new Detalle_pedidoDAO ();
            dao.create(objP);
        }
        return rta;
    }
    
    //Metodo para mostrar todos los pedidos:
    public List<Detalle_pedidoDTO> listarDetalle_pedidos()
    {
        List<Detalle_pedidoDTO> list = null;
        Detalle_pedidoDAO dao = new Detalle_pedidoDAO();
        list = dao.readAll();
        return list;
    }
}
