/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class LoginCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
       String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
       Facade objf = new Facade ();
        
        switch (accion)
        {
             case "Registrarse":
                  request.getRequestDispatcher("Registrarse.jsp").forward(request,response);             
             break;  
             case "IniciarSesion":
                  request.getRequestDispatcher("LoginVTA.jsp").forward(request,response);             
             break;    
             
            case "ValidaLogin":
            String correo = request.getParameter("txt_usuario");
            String clave = request.getParameter("txt_clave");
            
            SesionDTO dto = new SesionDTO ();
            
            dto.setCorreo(correo); //El correo sera el usuario de acceso
            dto.setClave(clave); 
            dto = objf.valida_credenciales(dto);
            
            //Me trae la lista de los usuarios para verificar su rol: 
            List<UsuarioDTO> lis = objf.listarUsuarios();
            //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
            List<SesionDTO> lis2 = objf.listarSesiones();
            
            int id=0;
            int band=0;
            for (int i=0; i<lis.size(); i++)
            {
                for (int j=0; j<lis2.size(); j++)
                {
                    if (lis2.get(j).getCorreo().equals(correo))
                    {
                        //Rescato el id que tiene registrado ese correo en sesiones:
                        id = lis2.get(j).getId_usu();
                    }                   
                }
                //Busco en la lista de usuarios ese id que rescate al que le pertenece el correo:
                if (lis.get(i).getId_usu() == id)
                {
                    if (lis.get(i).getRol().equals("Cliente"))
                    {
                        band = 1; //Es cliente
                    }
                    else
                    {
                        band = 2; //Es Administrador
                    }
                }
            }
            
            //VALIDACIÓN PARA ENTRAR COMO CLIENTE O ADMINISTRADOR
            
            if (band == 1) //Entra como cliente:
            {
                if (dto != null)
                {
                    System.out.println("Usuario y Contraseña validos!");
                    //Se crea la sesion:
                    HttpSession objLogin = request.getSession();
                    objLogin.setAttribute("login", dto); //Concateno la sesion con los datos del usuario.

                    //Como ya se valido la sesion entonces se redirecciona, es decir, se da permiso de que entre:
                    request.getRequestDispatcher("InterfazCliente.jsp").forward(request, response); 
                }
                else
                {
                    System.out.println("Usuario o Contraseña invalido...");
                    //Para que redireccione al error y le diga que no existe
                    request.getRequestDispatcher("Error_sesion.jsp").forward(request, response); 
                }
            }
            else //Entra como administrador:
            {
                if (dto != null)
                {
                    System.out.println("Usuario y Contraseña validos!");
                    //Se crea la sesion:
                    HttpSession objLogin = request.getSession();
                    objLogin.setAttribute("login", dto); //Concateno la sesion con los datos del usuario.

                    //Como ya se valido la sesion entonces se redirecciona, es decir, se da permiso de que entre:
                    request.getRequestDispatcher("InterfazAdmin.jsp").forward(request, response); 
                }
                else
                {
                    System.out.println("Usuario o Contraseña invalido...");
                    //Para que redireccione al error y le diga que no existe
                    request.getRequestDispatcher("Error_sesion.jsp").forward(request, response); 
                }
            }           
            break;
            
            case "Salir":
                HttpSession objLogin = request.getSession(); //Se obtiene la sesion
                objLogin.invalidate();
                request.getRequestDispatcher("index.jsp").forward(request, response); 
            break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
