/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.dto.Detalle_pedidoDTO;
import modelo.dto.PedidoDTO;
import modelo.dto.ProductoDTO;
import modelo.dto.SesionDTO;
import modelo.dto.UsuarioDTO;

/**
 *
 * @author JONATHAN
 */
public class ProductoCTO extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   // int cantidad_und;
    int id_prod;
    int id_pedido;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {   
         String menu = request.getParameter("menu"); //Indica que proceso o modulo gestionaria.
         String accion = request.getParameter("accion");  //Indica la accion a ejecutar. 
         
         
         if (menu.equalsIgnoreCase("Producto"))
         {
             Facade obj = new Facade();
             switch (accion)
             {
                 
                 //Para mostrar todos los productos:
                 case "Listar":
                    List<ProductoDTO> lis = obj.listarProductos();
                    request.setAttribute("Lista_productos", lis);
                 break;
                 
                 //Para guardar la cantidad de productos que se va agregar al carrito
                 case "Guardar":
                     int cantidad_und = Integer.parseInt(request.getParameter("txt_unidades"));
                     System.out.println("Unidades:" + cantidad_und); 
                     //Se crea un nuevo detalle_pedido:
                     Detalle_pedidoDTO nuevo1 = new Detalle_pedidoDTO (id_pedido, id_prod, cantidad_und);
                     obj.crearDetalle_pedido(nuevo1);                 
                     //Se lista el nuevo detalle_pedido:
                     List<Detalle_pedidoDTO> lisDetalle = obj.listarDetalle_pedidos();
                     request.getRequestDispatcher("Agregado.jsp").forward(request,response);
                 break;    
                 
                 //Para añadir productos al carrito de compras:
                 case "Agregar":   
                    //Obtengo el id del usuario que esta en la sesion:
                    HttpSession objLogin = request.getSession(); //Se obtiene el objeto de la sesion
                    SesionDTO usuRegistrado = (SesionDTO) objLogin.getAttribute("login"); //Se obtiene el usuario que entra a la sesion                    
                    Facade objf = new Facade ();
                    
                    //Me trae la lista de las sesiones para econtrar el usuario al que pertenece el correo: 
                    List<SesionDTO> lisS = objf.listarSesiones();
                    int id_usu=0;
                    
                    for (int j=0; j<lisS.size(); j++)
                    {
                        if (lisS.get(j).getCorreo().equals(usuRegistrado.getCorreo()))
                        {
                            //Rescato el id que tiene registrado ese correo en sesiones:
                            id_usu = lisS.get(j).getId_usu();
                        }                   
                    }

                    
                    //Revisar el estado del id del usuario que va hacer el pedido:
                    
                    //Me trae la lista de pedidos:
                    List<PedidoDTO> lisPe = obj.listarPedidos();
                    String est="";
                    
                    for (int i=0; i<lisPe.size(); i++)
                    {
                        //Encuentra en los pedidos el id del usuario:
                        if (lisPe.get(i).getId_usu() == id_usu)
                        {
                            est = lisPe.get(i).getEstado(); //Capturo el estado de ese usuario
                        }
                    }
                    
                    //Si el estado del pedido de ese usuario es seleccionado:
                    if (est.equals("Seleccionado"))
                    {
                        //No se crea pedido porque ya tiene y no ha finalizado
                        System.out.println("Ya tiene un pedido activo...");
                    }
                    else //Se crea un nuevo pedido
                    {
                        //Creo el pedido:
                        java.util.Date fecha = new Date(); //Fecha actual
                        int valor_total = 0;
                        String observaciones = "";
                        String estado = "Seleccionado";

                        PedidoDTO nuevo = new PedidoDTO(id_usu, fecha, valor_total, observaciones, estado);
                        //Se agrega un nuevo pedido:
                        obj.crearPedido(nuevo);
                        //Se lista ese nuevo pedido:
                        List<PedidoDTO> lisP = obj.listarPedidos();                        
                    } 
                    
                    /////////////////////////////////////////////////////////////////////
                    //CAPTURO LOS DATOS DE LA TABLA DETALLE_PEDIDO
                    //Guardo el id del producto que el usuario añadio al carrito:
                    List<PedidoDTO> lisPedi = obj.listarPedidos(); //Actualizo la lista de pedidos
                    id_prod = Integer.parseInt(request.getParameter("id")); 
                    id_pedido=0;
                    
                    //Guardo el id del pedido de ese usuario:
                    for (int i=0; i<lisPedi.size(); i++)
                    {
                        //Encuentra en los pedidos el id del usuario:
                        if (lisPedi.get(i).getId_usu() == id_usu)
                        {
                            id_pedido = lisPedi.get(i).getId_pedido();
                        }
                    }
                                       
                    request.getRequestDispatcher("Pregunta.jsp").forward(request,response); //Redirecciona al controlador
                 break;
             }
             request.getRequestDispatcher("Producto2VTA.jsp").forward(request,response);
         }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
