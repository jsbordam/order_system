<%-- 
    Document   : AdminProducto
    Created on : 24/08/2020, 06:00:31 PM
    Author     : JONATHAN
--%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>JSP Page</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div style="margin-top:20px" class="row">
                <div class="col-sm-12">
                    <center>
                        <font face="georgia" size="30" color="green">Gestiona los PRODUCTOS</font>
                    </center>                  
                </div>
            </div>
            <div style="margin-top:20px" class="row">             
                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de ingreso de datos--%>
                    <div class="card-body">
                        <form action="CuentaAdminCTO?menu=Producto" method="POST"> 
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Nombre Producto:</label>
                                    <input class="form-control" type="text" name="txt_nombre_pro" required value="${producto.getNombre_pro()}">
                                    <input type="hidden" name="txt_id_pro" value="${producto.getId_pro()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Descripcion Producto:</label>
                                    <input class="form-control" type="text" name="txt_descripcion_pro" required value="${producto.getDescripcion_pro()}"> 
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Imagen:</label>
                                    <input class="form-control" type="text" name="txt_imagen" required value="${producto.getImagen()}">
                                </div>   

                                <div class="form-group col-sm-3">
                                    <label>Unidades Disponibles:</label>
                                    <input class="form-control" type="text" name="txt_und_dis" required value="${producto.getUnd_dis()}">
                                </div>
                            </div>


                            <div class="form-row">


                                <div class="form-group col-sm-3">
                                    <label>Valor Producto:</label>
                                    <input class="form-control" type="text" name="txt_valor_pro" required value="${producto.getValor_pro()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Porcentaje Iva:</label>
                                    <input class="form-control" type="text" name="txt_iva_pro" required value="${producto.getIva_pro()}">
                                </div>
                            </div>

                            <c:if test = "${producto.getValor_pro() == null}">
                                <input class="btn btn-success" type="submit" name="accion" value="Agregar">               
                            </c:if>
                            <c:if test = "${producto.getValor_pro() != null}">
                                <input class="btn btn-warning" type="submit" name="accion" value="Actualizar">
                            </c:if>
                        </form>
                    </div>
                </div>                

                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de datos PRODUCTO--%>
                    <table class="table table-hover">
                        <thead>
                            <tr class="bg-success"> 
                                <th>Imagen</th>
                                <th>Id Producto</th>
                                <th>Nombre</th>
                                <th>Descripci�n</th>
                                <th>Unidades Disponibles</th>
                                <th>Valor</th>
                                <th>Porcentaje IVA</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="objP" items="${Lista_productos}">
                                <tr>
                                    <td>
                                        <img src="${objP.getImagen()}" height=100 width=100">                                       
                                    </td>
                                    <td>${objP.getId_pro()}</td>
                                    <td>${objP.getNombre_pro()}</td>
                                    <td>${objP.getDescripcion_pro()}</td>
                                    <td>${objP.getUnd_dis()}</td>
                                    <td>${objP.getValor_pro()}</td> 
                                    <td>${objP.getIva_pro()}</td>
                                    <td>     
                                        <a class="btn btn-success" href="CuentaAdminCTO?menu=Producto&accion=Editar&id=${objP.getId_pro()}">Editar</a> 
                                        <a class="btn btn-danger" href="CuentaAdminCTO?menu=Producto&accion=Eliminar&id=${objP.getId_pro()}">Eliminar</a>                                  
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>                     
                </div>
            </div> 
            <%--Esta linea es para utilizar codigo JavaScript--%>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        </div>
    </body>
</html>
