<%-- 
    Document   : index
    Created on : 20/08/2020, 08:49:19 PM
    Author     : JONATHAN
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>CCJ LICORES</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>
        <img src="https://images6.alphacoders.com/343/thumb-1920-343054.jpg" style="filter: blur(3px);width: 100vw; max-height: 100vh; position: absolute; z-index: -1;">
        <nav class="navbar navbar-expand-sm navbar-ligth bg-dark"> 
            <div id="navbarNav" class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">                       
                        <a class="btn btn-outline-success" style="margin-left: 10px;border: none" href="Inicio.jsp" target="Frame">Inicio</a>                   
                    </li>

                    <li class="nav-item">
                        <a class="btn btn-outline-success" style="margin-left: 10px;border: none"  href="Producto1VTA.jsp" target="Frame">Nuestros Productos</a>
                    </li > 
                    
                    <li class="nav-item">                       
                        <a class="btn btn-outline-success" style="margin-left: 10px;border: none" href="Mision.jsp" target="Frame">Mision</a>                   
                    </li>
                    
                    <li class="nav-item">                       
                        <a class="btn btn-outline-success" style="margin-left: 10px;border: none" href="Vision.jsp" target="Frame">Vision</a>                   
                    </li>
                    
                    <li class="nav-item">                       
                        <a class="btn btn-outline-success" style="margin-left: 10px;border: none" href="#">Contactenos</a>                   
                    </li>
                </ul>
            </div>
            <div class="dropdown">
                 <li class="nav-item">
                        <a class="btn btn-outline-primary" style="margin-right: 10px;border: none"  href="LoginCTO?accion=IniciarSesion" target="_blank">Iniciar Sesi�n</a>
                    </li>
                    
                     <li class="nav-item">
                        <a class="btn btn-outline-warning" style="margin-right: 10px;border: none"  href="LoginCTO?accion=Registrarse" target="_blank">Registrarse</a>
                    </li> 
            </div>
        </nav> 
        
        
        <%--Esta linea es para navegar en la misma pesta�a y que no se cree otra al pasar a una vista--%>
         <div style="height: 830px;">
            <iframe name="Frame" style="height: 100%;width: 100%;border: 2px"></iframe>
        </div>
     
        <%--Esta linea es para utilizar codigo JavaScript--%>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
