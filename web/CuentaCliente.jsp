<%-- 
    Document   : CuentaCliente
    Created on : 24/08/2020, 12:36:15 PM
    Author     : JONATHAN
--%>

<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-15"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
        <%--Esta linea es para usar Boostrap--%>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>JSP Page</title>
        <%--Esta linea me a�ade hojas de estilo de CCS --%>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    <body>        
        <div class="container">
            <div style="margin-top:20px" class="row">
                <div class="col-sm-12">
                    <center>
                        <font face="georgia" size="30" color="blue">Gestiona tus datos</font>
                    </center>                                              
                </div>
            </div>
            <div style="margin-top:20px" class="row">
                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de ingreso de datos--%>
                    <div class="card-body">
                        <form action="CuentaClienteCTO?menu=Cuenta" method="POST">                                                 
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Nombres:</label>
                                    <input class="form-control" type="text" name="txt_nombres" required value="${usuario.getNombres()}">
                                    <input type="hidden" name="txt_id_usu" value="${usuario.getId_usu()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Apellidos:</label>
                                    <input class="form-control" type="text" name="txt_apellidos" required value="${usuario.getApellidos()}"> 
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="inputState">Tipo Documento:</label>
                                    <select id="inputState" class="form-control" name="txt_tipo_docto" required value="${usuario.getTipo_docto()}">
                                        <option>Cedula de ciudadania</option>
                                        <option>Tarjeta de identidad</option>
                                        <option>Carnet de extranjeria</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>N� Documento:</label>
                                    <input class="form-control" type="text" name="txt_num_docto" required value="${usuario.getNum_docto()}">
                                </div>                                 
                            </div>

                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label>Ciudad de residencia:</label>
                                    <input class="form-control" type="text" name="txt_ciudad_residencia" required value="${usuario.getCiudad_residencia()}"> 
                                </div>

                                <div class="form-group col-sm-3">
                                    <label>Direcci�n:</label>
                                    <input class="form-control" type="text" name="txt_direccion" required value="${usuario.getDireccion()}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="inputState">Rol:</label>
                                    <select id="inputState" class="form-control" name="txt_rol" required value="${usuario.getRol()}">                
                                        <option>Cliente</option>
                                    </select>
                                </div>                                                       
                            </div>

                            <input class="btn btn-primary" type="submit" name="accion" value="Actualizar">
                        </form>
                    </div>
                </div>

                <div class="card col-sm-12"> <%--Para darle bordes a la tabla de datos--%>
                    <table class="table table-hover table-hover col-sm-12">
                        <thead>
                            <tr class="bg-primary">
                                <th>N� Id</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Tipo documento</th>
                                <th>Numero documento</th>
                                <th>Ciudad residencia</th>
                                <th>Direccion</th>
                                <th>Rol</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="objP" items="${Lista_usuario}">
                                <tr>
                                    <td>${objP.getId_usu()}</td>
                                    <td>${objP.getNombres()}</td>
                                    <td>${objP.getApellidos()}</td>
                                    <td>${objP.getTipo_docto()}</td>
                                    <td>${objP.getNum_docto()}</td>
                                    <td>${objP.getCiudad_residencia()}</td>
                                    <td>${objP.getDireccion()}</td>
                                    <td>${objP.getRol()}</td>
                                    <td>
                                        <a class="btn btn-primary" href="CuentaClienteCTO?menu=Cuenta&accion=Editar&id=${objP.getId_usu()}">Editar</a> 
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>            
                    </table>
                </div>
            </div>
            <%--Esta linea es para utilizar codigo JavaScript--%>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        </div>
    </body>
</html>
